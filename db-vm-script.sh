#!/bin/bash

# Update package lists
sudo apt-get update

# Install PostgreSQL and its dependencies
sudo apt-get install -y postgresql postgresql-contrib

# Start the PostgreSQL service
sudo systemctl start postgresql.service

# Create a PostgreSQL database
sudo -u postgres psql -c "CREATE DATABASE flask_db;"

# Create a PostgreSQL user and grant privileges on the database
sudo -u postgres psql -c "CREATE USER moshe WITH PASSWORD 'moshe';"
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE flask_db TO moshe;"


echo "listen_addresses = '*'" | sudo tee -a /etc/postgresql/*/main/postgresql.conf
echo "host   all    all    10.0.1.0/24     md5" | sudo tee -a /etc/postgresql/*/main/pg_hba.conf
echo "host    flask_db    moshe   10.0.1.4/32    md5" | sudo tee -a /etc/postgresql/*/main/pg_hba.conf

sudo ufw allow 5432/tcp
sudo service postgresql restart
sudo systemctl start postgresql.service

#go into flask_db 
sudo -u postgres psql -d flask_db

